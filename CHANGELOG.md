1.1.0
-----------------------------------------------------------------------------
- Update `network` dependency (see MR #5, thanks to @axeman).

1.0.0
-----------------------------------------------------------------------------
- Drop support for GHC < 8.0.
- Require redis-resp >= 1.0.

0.7.0
-----------------------------------------------------------------------------
- `pipelined` has been renamed to `commands`.
- `stepwise` is gone (use `sync` to force immediate sending of commands).
- `transactional` is gone (transactions have to be used explicitly with
  `MULTI` and `EXEC`).

0.6.0
-----------------------------------------------------------------------------
- The `TransactionFailure` type distinguishes more cases.
- Bugfixes:
    https://gitlab.com/twittner/redis-io/merge_requests/1

0.5.2
-----------------------------------------------------------------------------
- Update test dependencies

0.5.1
-----------------------------------------------------------------------------
- Bugfixes

0.5.0
-----------------------------------------------------------------------------
- `transactional` has been added to execute redis transactions.
- Bugfixes

0.4.1
-----------------------------------------------------------------------------
- Support `monad-control` 1.*
