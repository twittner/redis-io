# redis-io

*Yet another Redis client.*

A redis client library interpreting [redis-resp][1] commands.

[1]: https://gitlab.com/twittner/redis-resp
